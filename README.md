# The Crescendo: A Smart Pre-War Safe House

## (with [Sortomatic](https://www.nexusmods.com/newvegas/mods/41142) compatibility)

*\[See changelogs for what has been added and fixed\]*

**!!! To upgrade from old to newer version, make sure to remove the old version, save your game, and then reload it with the new version !!!**

### Description

This mod adds a safe house near Novac fully functional, containing

- an armoury,
- an infirmary (with an improved auto-doc and an advance chemistry set),
- a living room,
- an office,
- two new weapons,
- lighting for each room,
- cabinets to store a multitude of things (weapons, armour, foods, ...),
- an incinerator,
- an online sales system,
- and lots of other surprises... *(See changelogs for curious)*


The safe house is near Novac (see pictures). A letter on the hatch of the safe house will explain how to acquire this little pre-war jewel.

Each room in the safe house can be improved with the main terminal (located next to the ladder inside).

### Sortomatic information

**!!! Since the update 1.5.1, Sortomatic is now optional for those who prefer not to use it**

- If you've never used Sortomatic before, I strongly advice you to watch [this video](https://www.youtube.com/watch?v=CCe2NxrEsNk) (in English) which explains very well how to use it

**!!! Since the update 1.3.1, all containers are automatically managed by default, BUT:**

- If Sortomatic is initialized (i.e., the display of the initialization message) **AFTER** the purchase of one or more containers, the default sort **WILL BE** managed *for those containers*.
- If Sortomatic is initialized (i.e., the display of the initialization message) **BEFORE** the purchase of one or more containers, the default sort **WILL NOT BE** managed *for those containers* *(because, at that time, the containers weren't visible)*.
- **To fix this problem**, you need to reset the containers managed by default by activating the **\[MSC\] Master Sorting Control activator** (see pictures),
- and then clicking on **Option ...**  >  **Reset to Designer Defaults ...**  >  **Confirm**

**xNVSE**

- **If you are using [xNVSE](https://github.com/xNVSE/NVSE/releases/)** : After Sortomatic is initialized (by purchasing the upgrade in the terminal), you **MUST save and then restart the game** for the initialization to be done correctly.


### Requirement

- Fallout: New Vegas
- DLC01 - Dead Money
- DLC02 - Honest Hearts
- DLC03 - Old World Blues
- DLC04 - Lonesome Road
- DLC05 - Gun Runners Arsenal
- [~~JIP LN NVSE Plugin~~](https://www.nexusmods.com/newvegas/mods/58277) (Only needed for v1.4.0 and below)

#### Sortomatic Plugin

- [New Vegas Script Extender (xNVSE)](https://github.com/xNVSE/NVSE/releases/) (tested with version 6.1.6 and below)
- [Sortomatic - Modders Resource (v1.3)](https://www.nexusmods.com/newvegas/mods/41142)

### Installation

1. Extract the files from the archive.
2. Copy the files into the **Data** folder where the game is installed (Default : *C:\Program Files (x86)Steam\SteamApps\common\fallout new vegas\Data*)
3. Start Fallout: New Vegas Launcher, click *Data Files*, place a check mark beside **Crescendo.esp** file.

OR

1. Download and install the mod with Vortex.
