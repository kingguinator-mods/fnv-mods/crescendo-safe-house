# CHANGELOG

## 1.5.1

### Redone

- Sortomatic is no longer required, and can be enabled optionally by installing an additional .esp file

## 1.5.0

### New

- New Location for the quest (avoid incompatibilities with Goodsprings House)
- Greenhouse (GH) Revisited
- GH - Multitude of Plants
- GH - Plants can growth with light and water
- GH - Plants need now time to be mature
- GH - Add New Blocked Path System
- GH - Decorations
- GH - Everything can be purchased from the terminals
- Generator and/or Pump may randomly shutdown unexpectedly (Based on Luck)
- All the Crescendo is influenced by the shutdown of one of the two facilities
- Need to repair Elevator
- Add a Machine to Sanitize Irradiated Items in the Infirmary
- Infirmary Sliding Door texture
- Add Jazz Radio
- Parameter accessible from sneaking + activate the main terminal
- Add Snowglobes for all DLCS
- Add All DLCs as Requirements

### Redone

- Quest to obtain the Crescendo Key
- Chemistry Set Behaviors and Features
- Change Frequencies behavior to avoid using JIP

### Fix

- Add COCMarker in all rooms
- Combine statics to create collections
- Player is the Owner of the Crescendo
- Size Boxes - Repair Generator + Pump
- Clipping Grounds + Walls
- Better Lights
- VendorTV Repair Level
- Rename and refactor all IDs to ease maintainability and readability

## 1.4.0

### New

- Add a new quest to obtain the Crescendo key and access to it
- Change radio frequency with a button next to Jukeboxes
- Add a boiler which can purify dirty and irradiated water
- Add an elevator with 2 new levels
- (Last Level) Need to restart machines to supplying the Crescendo with electricity and water
- (Level 2) Add a greenhouse
- Data compiled in BSA file (for the release)
- English and French versions compiled into a single archive (FOMOD) (for the release)

### Redone

- (Sub-)Terminal Renamed

### Fix

- Radio switch frequency correctly
- Increase performance by deleting statics which is disabled when player buys new furnitures
- Increase performance by removing the snow in snowglobes
- Increase performance by deleting statics requiring a lot of memory

## 1.3.2

### New

- NavMeshes added
- Player can change radio station when he/she activates a radio/jukebox and he/she is crouched down (Only in the Crescendo)
- Snow globe Display Stand to add snow globes in the Crescendo
- Godfather's suit with unique textures

### Redone

- Better Chemistry Set
- Better ladders and front hatch
- Crescendo Sale System dialogues fixed
- Meshes and Textures folders

### Fix

- Goodbye bug with some NPCs
- Scripts errors

## 1.3.1

### Redone

- Better Auto-Doc

### Fix

- [Sortomatic] All containers are managed by default (no need to specify the container type yourself)

- Adjustment of some inconsistencies

## 1.3.0

### New

- Add Sortomatic compatibility
- Add two fridges and a Nuka Cola fridge/vending machine

### Redone

- Retexture for rugs and others furnitures
- Slightly extend the entrance hall

## 1.2.0

### New

- First public release on Nexus